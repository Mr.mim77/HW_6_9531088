package aut.ir.homework6;



/**
 * Created by mohsen on 5/12/17.
 */
public class MonsterCard extends Card{
    private int power , basePower ;
    private boolean canAttack;
    public MonsterCard(String name , String description, int power)
    {
        this(name , description,power, false);
    }
    MonsterCard(String name, String description, int power, boolean canAttack)
    {
        super(name, description);
        this.power = power;
        this.canAttack = canAttack;
    }

    @Override
    public boolean equals(Card card) {
        return super.equals(card) && power ==((MonsterCard) card).getPower() && basePower == ((MonsterCard) card).getBasePower();
    }

    @Override
    public String toString() {
        return super.toString()+ "| Power: "  + power + "| Can attack:" + canAttack ;
    }

    public int getPower()
    {
        return power;
    }
    public int getBasePower()
    {
        return basePower;
    }
    public boolean getCanAttack()
    {
        return canAttack;
    }
    public void setPower(int power)
    {
        this.power = power;
    }
    public void setCanAttack(boolean canAttack)
    {
        this.canAttack = canAttack;
    }

}

