package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public class BlueEyesWhiteDragon extends MonsterCard implements Special{
    public BlueEyesWhiteDragon() {
        super("Blue Eyes White Dragon","The best card",3000,true);
    }

    @Override
    public void instantEffect(Field owner, Field enemy) {
        owner.addMonsterCard(this);
        owner.addMonsterCard(this);
    }
}
