package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public abstract class TrapCard extends Card implements Special {
    public TrapCard(String name, String description) {
        super(name, description);
    }

    @Override
    public boolean equals(Card card) {
        return super.equals(card) && this.getClass().getName().equals(card.getClass().getName());
    }
}
