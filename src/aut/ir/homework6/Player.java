package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public class Player {
    CardDeck mainDeck;
    SpecialDeck specialDeck;
    Card[] hand = new Card[5];
    Special nextSpecial;
    int lifePoints;

    public Player(CardDeck mainDeck, SpecialDeck specialDeck, int lifePoints) {
        this.mainDeck = mainDeck;
        this.specialDeck = specialDeck;
        this.lifePoints = lifePoints;
    }

    public Player(CardDeck mainDeck, SpecialDeck specialDeck) {
        this.mainDeck = mainDeck;
        this.specialDeck = specialDeck;
        this.lifePoints = 5000;
    }


    public CardDeck getMainDeck() {
        return mainDeck;
    }

    public SpecialDeck getSpecialDeck() {
        return specialDeck;
    }

    public Card[] getHand() {
        return hand;
    }

    public Special getNextSpecial() {
        return nextSpecial;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setCardDeck(CardDeck mainDeck) {
        this.mainDeck = mainDeck;
    }

    public void setSpecialDeck(SpecialDeck specialDeck) {
        this.specialDeck = specialDeck;
    }

    public void setHand(Card[] hand) {
        this.hand = hand;
    }

    public void setNextSpecial(Special nextSpecial) {
        this.nextSpecial = nextSpecial;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }
    public boolean draw(int count)
    {
        if(mainDeck.size() < count) return false;
        for(int i = 0;i<5 && count > 0;i++)
        {
            if(hand[i] == null) {
                hand[i] = mainDeck.deal();
                if (hand[i] == null) return false;
                count--;
            }
        }
        return true;
    }
    public void drawSpecialCard()
    {
        if(nextSpecial == null)
            nextSpecial = (Special) specialDeck.deal();
    }
    public void nextTurnPrep()
    {
        if(draw(1) == false) changeLifePoints(500);
        drawSpecialCard();
    }
    public boolean isDefeated()
    {
        return lifePoints<=0;
    }
    public void changeLifePoints(int change)
    {
        lifePoints+=change;
    }
    public boolean playCardFromHand(int whichCard, Field myField)
    {
        if(whichCard <5 && whichCard >=0)
        {
            if(hand[whichCard] == null)
                return false;
            if(hand[whichCard].getClass().getName().equals(MonsterCard.class.getName())) {
                if (myField.addMonsterCard((MonsterCard) hand[whichCard])) {
                    hand[whichCard] = null;
                    return true;
                }
                else
                    return false;
            }
            else if(hand[whichCard].getClass().getName().equals(SpellCard.class.getName())) {
                if(myField.addSpellCard((SpellCard) hand[whichCard])) {
                    hand[whichCard] = null;
                    return true;
                }
                else
                    return false;
            }
        }
        return false;
    }
    boolean playSpecial(Field myField)
    {
        if(nextSpecial == null) return false;
        nextSpecial.instantEffect(myField,null);
        return false;
    }

}
