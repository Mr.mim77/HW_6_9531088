package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public class SpecialDeck extends ObjectDeck {
    public SpecialDeck(Card... cardArray) {
        super(cardArray);
    }
    public Card deal()
    {
        return (Card)super.deal();
    }
}

