package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public class Field {
    private MonsterCard[] monsters = new MonsterCard[5];
    private SpellCard[] spells = new SpellCard[5];

    public MonsterCard[] getMonsters() {
        return monsters;
    }
    public void setMonster(MonsterCard monster,int index)
    {
        monsters[index] = monster;
    }
    void cardTurnEffects(Field enemyField)
    {
        for(int i=0;i<5;i++)
        {
            monsters[i].setCanAttack(true);
        }
        for(int i=0;i<5;i++)
            if(spells[i]!=null)
                spells[i].turnEffect(this,enemyField);
    }
    public void destroySpellCards(Field enemyField)
    {
        for(int i=0;i<5;i++) {
            if(spells[i]!=null)
                spells[i].destroyedEffect(this,enemyField);
            spells[i] = null;
        }
    }
    boolean addMonsterCard(MonsterCard card)
    {
        for(int i=0;i<5;i++)
            if(monsters[i] == null) {
                monsters[i] = card;
                return true;
            }
            return false;
    }
    boolean addSpellCard(SpellCard card)
    {
        for(int i=0;i<5;i++)
            if(spells[i] == null)
            {
                spells[i] = card;
                return true;
            }
            return false;
    }
    public SpellCard[] getSpells() {
        return spells;
    }


}
