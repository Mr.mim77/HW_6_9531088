package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public class DestroySpell extends TrapCard {
    public DestroySpell() {
        super("Destroy Spells ", "Destroys the enemy’s first spell card." );
    }

    @Override
    public void instantEffect(Field owner, Field enemy) {
        enemy.destroySpellCards(owner);
    }
}
