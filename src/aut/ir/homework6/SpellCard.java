package aut.ir.homework6;

import aut.ir.homework6.Card;

/**
 * Created by mohsen on 5/12/17.
 */
public abstract class SpellCard extends Card {
    public SpellCard(String name, String description) {
        super(name, description);
    }
    //TODO
    abstract void turnEffect(Field ownerField, Field enemyField);
    abstract void destroyedEffect(Field ownerField, Field enemyField);

    @Override
    public boolean equals(Card card) {
        return super.equals(card) && this.getClass().getName().equals(card.getClass().getName());
    }
}
