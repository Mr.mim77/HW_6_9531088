package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public interface Special {
    void instantEffect(Field owner, Field enemy);
}
