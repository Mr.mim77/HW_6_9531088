package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public abstract class Card {
    private String name;
    private String description;
    public Card(String name , String description)
    {
        this.name = name;
        this.description = description;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getName()
    {
        return name;
    }
    public String getDescription()
    {
        return description;
    }

    @Override
    public String toString() {
        return name + ": " + description;
    }

    public boolean equals(Card card) {
        return name.equals(card.getName()) && description.equals(card.getDescription());
    }
}
