package aut.ir.homework6;

/**
 * Created by mohsen on 5/12/17.
 */
public abstract class ObjectDeck {
    private Object[] objectArray;
    private int arraySize;
    public ObjectDeck(Object... input)
    {
        arraySize = input.length;
        objectArray = new Object[arraySize];
        for(int i=0;i<input.length;i++)
            objectArray[i] = input[i];

    }
    public int size()
    {
        return arraySize;
    }
    public boolean isEmpty()
    {
        return size() == 0;
    }
    public Object deal()
    {
        if(arraySize>0) {
            arraySize--;
            return objectArray[arraySize - 1];
        }
        return null;
    }
}

